# Branch and Bound for the Independant Set Problem.

We are interested in a classical combinatorial problem in this project: the Independant set problem.

We implement and experiment approximation heuristics with prooved guarantees that provide us lower and upper bound on the optimal solution.

The objective was to study the trade-off between the efficiency of a heuristic in computing a tight bond and its time consumption.
